#!/bin/bash
set -e

cd /home/dallas/work/web-projects/km-python-test/src/
source /opt/virtualenv/km-python-test/bin/activate

exec ./manage.py celeryd -B
