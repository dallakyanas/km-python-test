#!/bin/bash
set -e
LOGFILE=/home/dallas/work/web-projects/km-python-test/log/gunicorn/kmtest.log
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=3
# user/group to run as
USER=dallas
GROUP=dallas
PORT=8001 # Этот порт будет разным у каждого django-проекта

cd /home/dallas/work/web-projects/km-python-test/src/
source /opt/virtualenv/km-python-test/bin/activate

test -d $LOGDIR || mkdir -p $LOGDIR

exec gunicorn kmtest.wsgi:application -w $NUM_WORKERS \
  --user=$USER --group=$GROUP --log-level=debug \
  --log-file=$LOGFILE 2>>$LOGFILE \
  --bind 127.0.0.1:$PORT


# exec ../bin/gunicorn_django -w $NUM_WORKERS \
#   --user=$USER --group=$GROUP --log-level=debug \
#   --log-file=$LOGFILE 2>>$LOGFILE \
#   --bind 127.0.0.1:$PORT