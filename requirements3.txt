Django==1.9.6
et-xmlfile==1.0.1
gunicorn==19.5.0
jdcal==1.2
openpyxl==2.3.5
Pillow==3.2.0
django-celery==3.1.17
django-kombu==0.9.4

