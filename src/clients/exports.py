import datetime

from openpyxl import Workbook
from openpyxl.styles import Font, Alignment

from .models import Client

# import mimetypes
# from django.http import StreamingHttpResponse
# from django.core.servers.basehttp import FileWrapper


def exportToExcel(_filename):
    wb = Workbook()
    ws = wb.active
    ws.title = "Client list"
    ws.column_dimensions['A'].width = 25
    ws.column_dimensions['B'].width = 25
    ws.column_dimensions['C'].width = 10
    ws.column_dimensions['D'].width = 15

    ws.row_dimensions[1].height = 30
    ws.row_dimensions[2].height = 30

    # report's title 1st line
    ws.merge_cells('A1:D1')
    ws['A1'] = "Clients list"

    ws['A1'].font = Font(name='Arial',
                         size=15,
                         bold=True)

    ws['A1'].alignment = Alignment(horizontal='center',
                                   vertical='center')

    # report's title 2nd line
    ws.merge_cells('A2:D2')
    ws['A2'] = datetime.datetime.now()

    ws['A2'].font = Font(name='Arial',
                         size=11)

    ws['A2'].alignment = Alignment(horizontal='center',
                                   vertical='center')

    ws.append([])

    # draw headers
    ws.append([
               "First Name",
               "Second Name",
               "Age",
               "Birth date",
               ])

    for row in ws['A4':'D4']:
        for c in row:
            c.font = Font(name='Arial',
                          size=13,
                          bold=True)

            c.alignment = Alignment(horizontal='center',
                                    vertical='center')

    # draw all query set rec by rec
    query_list = Client.objects.all()

    for rec in query_list:
        ws.append([
                   rec.first_name,
                   rec.second_name,
                   rec.age,
                   rec.birth_date,
                   ])

    wb.save(_filename)
