from django.conf.urls import url

# from . import views

from .views import (
    client_list,
    client_create,
    client_detail,
    client_update,
    client_delete,
    download_report,
    vote_for_client,
    vote_for_client_poll,
    client_poll,
    )

urlpatterns = [
    url(r'^$', client_list, name="list"),
    url(r'^report/$', download_report, name="download_report"),
    url(r'^create/$', client_create, name='create'),
    url(r'^poll/$', client_poll, name='poll'),
    url(r'^detail/(?P<id>\d+)/$', client_detail, name='detail'),
    url(r'^detail/(?P<id>\d+)/delete/$', client_delete, name='delete'),
    url(r'^detail/(?P<id>\d+)/update/$', client_update, name='update'),
    url(r'^detail/(?P<id>\d+)/vote/$', vote_for_client, name='vote_for_client'),
    url(r'^poll/(?P<id>\d+)/vote/$', vote_for_client_poll, name='vote_for_client_poll'),
]
