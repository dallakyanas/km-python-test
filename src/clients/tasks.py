from __future__ import absolute_import

from celery import shared_task

from django.shortcuts import get_object_or_404

from .exports import exportToExcel
from .models import Client


@shared_task
def generate_report(_filename):
    exportToExcel(_filename)


@shared_task
def like_the_client(id):
    instance = get_object_or_404(Client, id=id)

    if instance.likes < 10:
        instance.likes = instance.likes + 1
        instance.save()

    return instance.likes
