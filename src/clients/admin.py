from django.contrib import admin

# Register your models here.

from .models import Client


class ClientAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'second_name',  'age', 'birth_date')
    search_fields = ['first_name', 'second_name',  'age', 'birth_date']


admin.site.register(Client, ClientAdmin)
