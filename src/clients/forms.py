import datetime
from django import forms

from .models import Client

CURRENT_YEAR = int(datetime.datetime.now().year)
BIRTH_YEAR_CHOICES = tuple(y for y in xrange(1900, CURRENT_YEAR, 1))


class ClientForm(forms.ModelForm):
    birth_date = forms.DateField(widget=forms.SelectDateWidget(years=BIRTH_YEAR_CHOICES))

    class Meta:
        model = Client
        fields = [
            "first_name",
            "second_name",
            "age",
            "birth_date",
            "photo"
        ]
