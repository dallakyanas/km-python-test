from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db import models

# Create your models here.


def upload_location(instance, filename):
    return "db-img/{}".format(filename)


class Client(models.Model):
    first_name = models.CharField(max_length=120)
    second_name = models.CharField(max_length=120)
    age = models.IntegerField(default=0)
    birth_date = models.DateField()
    photo = models.ImageField(upload_to=upload_location,
                              null=True,
                              blank=True)
    likes = models.IntegerField(default=0)  # FIXME: add constraint 0 <= likes <= 10

    def __unicode__(self):
        return u"{} {}".format(self.first_name, self.second_name)

    def __str__(self):
        return "{} {}".format(self.first_name, self.second_name)

    def get_absolute_url(self):
        return reverse("clients:detail", kwargs={"id": self.id})

    class Meta:
        ordering = ["first_name", "second_name"]
